import React, { useRef, useState, useEffect } from "react";
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Switch,
  Modal,
  Dimensions,
  Image,
  TouchableOpacity
} from "react-native";
import { PermissionsAndroid, Platform } from "react-native";
import {
  ClientRoleType,
  createAgoraRtcEngine,
  IRtcEngine,
  RtcSurfaceView,
  ChannelProfileType,
  VideoSourceType,
  RenderModeType,
  IRtcEngineEx,
  ErrorCodeType
} from "react-native-agora";
import imgMute from './src/assets/mute.png'
import imgUnMute from './src/assets/unmute.png'
import imgStop from './src/assets/stop.png'

const { width, height } = Dimensions.get('window')

const appId = "63389e1d11ab405a8a66faf9973b2fb6";
const channelName = "LiveStreamDemoChannel";
const uid = 0;

const App = () => {
  const [token, setToken] = useState("007eJxTYFjQOnFx81lJwy9MWdX2k/4sDl+4vlKytuFJ2WJFEZeY14IKDGbGxhaWqYYphoaJSSYGpokWiWZmaYlplpbmxklGaUlmtQwtKQ2BjAw888QYGRkgEMQXZfDJLEsNLilKTcx1Sc3Nd85IzMtLzWFgAABXjCRm")
  const [isMuted, setIsMuted] = useState(false)

  const agoraEngineRef = useRef<IRtcEngineEx>(); // Agora engine instance
  const [isJoined, setIsJoined] = useState(false); // Indicates if the local user has joined the channel
  const [isHost, setIsHost] = useState(true); // Client role
  const [remoteUid, setRemoteUid] = useState(0); // Uid of the remote user
  const [message, setMessage] = useState(""); // Message to the user

  const getPermission = async () => {
    if (Platform.OS === "android") {
      await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        PermissionsAndroid.PERMISSIONS.CAMERA,
      ]);
    }
  };

  function showMessage(msg: string) {
    setMessage(msg);
  }

  useEffect(() => {
    // Initialize Agora engine when the app starts
    setupVideoSDKEngine();
  }, []);

  const setupVideoSDKEngine = async () => {
    try {
      // use the helper function to get permissions
      agoraEngineRef.current = createAgoraRtcEngine();
      const agoraEngine = agoraEngineRef.current;
      agoraEngine.registerEventHandler({
        onJoinChannelSuccess: () => {
          showMessage('Successfully joined the channel ' + channelName);
          setIsJoined(true);
        },
        onUserJoined: (_connection, Uid) => {
          showMessage('Remote user joined with uid ' + Uid);
          setRemoteUid(Uid);
        },
        onUserOffline: (_connection, Uid) => {
          showMessage('Remote user left the channel. uid: ' + Uid);
          setRemoteUid(0);
        },
      });
      agoraEngine.initialize({
        appId: appId,
        channelProfile: ChannelProfileType.ChannelProfileLiveBroadcasting,
      });
      agoraEngine.enableVideo();
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    if (isHost) {
      agoraEngineRef.current?.muteLocalAudioStream(isMuted);
    } else {
      agoraEngineRef.current?.muteAllRemoteAudioStreams(isMuted)
    }
  }, [isMuted])

  const mute = () => {
    setIsMuted(!isMuted)
  };

  const join = async () => {
    if (isJoined) {
      return;
    }
    try {
      if (Platform.OS === 'android') { await getPermission() };
      agoraEngineRef.current?.setChannelProfile(
        ChannelProfileType.ChannelProfileLiveBroadcasting,
      );
      if (isHost) {
        agoraEngineRef.current?.startPreview();
        agoraEngineRef.current?.joinChannel(token, channelName, uid, {
          clientRoleType: ClientRoleType.ClientRoleBroadcaster
        });
      } else {
        agoraEngineRef.current?.joinChannel(token, channelName, uid, {
          clientRoleType: ClientRoleType.ClientRoleAudience
        });
      }
    } catch (e) {
      console.log(e);
    }
  };


  const leave = () => {
    try {
      agoraEngineRef.current?.leaveChannel();
      setRemoteUid(0);
      setIsJoined(false);
      showMessage('You left the channel');
    } catch (e) {
      console.log(e);
    }
  };

  const renderControl = () => {
    return (
      <View style={{ position: 'absolute', bottom: Platform.OS === "android" ? 20 : 30, zIndex: 11, backgroundColor: '#000000fa', alignSelf: 'center', width: '100%', padding: 10 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
          <TouchableOpacity style={styles.btnRound} onPress={mute}>
            <Image source={isMuted ? imgMute : imgUnMute} style={{ height: 20, width: 20 }} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnRound} onPress={leave}>
            <Image source={imgStop} style={{ height: 20, width: 20 }} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  const renderVideo = () => {
    return (
      <View style={{ height: height, width: width, position: 'absolute', top: 0, right: 0, left: 0, bottom: 0, zIndex: 10, backgroundColor: 'black' }}>
        {isJoined && isHost ? (
          <RtcSurfaceView canvas={{
            uid: 0,
          }} style={styles.videoView} />
        )
          :
          remoteUid !== 0 ?
            <RtcSurfaceView
              canvas={{ uid: remoteUid }}
              style={styles.videoView}
            />
            : <Text>
              {isJoined && !isHost ? "Waiting for a remote user to join" : ""}
            </Text>
        }
        {
          renderControl()
        }
      </View>
    )
  }


  return (
    <SafeAreaView style={styles.main}>
      <Text style={styles.head}>
        Agora Interactive Live Streaming Quickstart
      </Text>
      <View style={styles.btnContainer}>
        <Text onPress={join} style={styles.button}>
          {isHost ? 'Start Live Streaming' : 'Join'}
        </Text>
      </View>
      <View style={styles.btnContainer}>
        <Text style={{ marginRight: 5 }}>Audience</Text>
        <Switch
          onValueChange={(switchValue) => {
            setIsHost(switchValue);
            if (isJoined) {
              leave();
            }
          }}
          value={isHost}
        />
        <Text style={{ marginLeft: 5 }}>Host</Text>
      </View>
      <Text>{isHost ? "Join a channel" : ""}</Text>

      <Text style={styles.info}>{message}</Text>
      {
        isJoined && renderVideo()
      }
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 25,
    paddingVertical: 4,
    fontWeight: 'bold',
    color: '#ffffff',
    backgroundColor: '#0055cc',
    margin: 5,
  },
  main: { flex: 1, alignItems: 'center' },
  scroll: { flex: 1, backgroundColor: '#ddeeff', width: '100%' },
  scrollContainer: { alignItems: 'center' },
  videoView: { height: '100%', width: '100%', },
  btnContainer: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center', },
  head: { fontSize: 20 },
  info: { backgroundColor: '#ffffe0', paddingHorizontal: 8, color: '#0000ff' },
  btnRound: {
    backgroundColor: 'white', height: 40, width: 40, borderRadius: 20, marginRight: 10,
    alignItems: 'center', justifyContent: 'center'
  }
});

export default App;
